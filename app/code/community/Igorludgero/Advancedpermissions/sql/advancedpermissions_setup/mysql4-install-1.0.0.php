<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 25/08/16
 * Time: 23:09
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Create table 'foo_bar_baz'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('ilpermissions_datetime'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'ID')
    ->addColumn('admin_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 0, array(
        'nullable'  => false,
    ), 'Admin User ID')
    ->addColumn('day_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 0, array(
        'nullable'  => false,
    ), 'Day ID')
    ->addColumn('hour_start', Varien_Db_Ddl_Table::TYPE_TIME, 0, array(
        'nullable'  => false,
    ), 'Hour Start')
    ->addColumn('hour_end', Varien_Db_Ddl_Table::TYPE_TIME, 0, array(
        'nullable'  => false,
    ), 'Hour End');

$installer->getConnection()->createTable($table);

$installer->endSetup();