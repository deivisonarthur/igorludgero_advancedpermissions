<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 26/08/16
 * Time: 17:24
 */

class Igorludgero_Advancedpermissions_Block_Adminhtml_Datetime_Renderer_Day extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        switch ($value){
            case 1:
                return Mage::helper("igorludgero_advancedpermissions")->__("Sunday");
            case 2:
                return Mage::helper("igorludgero_advancedpermissions")->__("Monday");
            case 3:
                return Mage::helper("igorludgero_advancedpermissions")->__("Tuesday");
            case 4:
                return Mage::helper("igorludgero_advancedpermissions")->__("Wednesday");
            case 5:
                return Mage::helper("igorludgero_advancedpermissions")->__("Thursday");
            case 6:
                return Mage::helper("igorludgero_advancedpermissions")->__("Friday");
            case 7:
                return Mage::helper("igorludgero_advancedpermissions")->__("Saturday");
        }

    }

}
?>