<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 26/08/16
 * Time: 13:53
 */

class Igorludgero_Advancedpermissions_Block_Adminhtml_Datetime_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        // Set some defaults for our grid
        $this->setDefaultSort('day_id');
        $this->setId('advancedpermissions_datetime_grid');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _getCollectionClass()
    {
        // This is the model we are using for the grid
        return 'advancedpermissions/datetime_collection';
    }

    protected function _prepareCollection()
    {
        // Get and set our collection for the grid
        $adminId = $this->getRequest()->getParam('user_id');
        $collection = Mage::getResourceModel($this->_getCollectionClass())->addFilter("admin_id",$adminId);
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        // Add the columns that should appear in the grid
        /*$this->addColumn('id',
            array(
                'header'=> $this->__('ID'),
                'align' =>'right',
                'width' => '50px',
                'index' => 'id'
            )
        );*/

        $this->addColumn('day_id',
            array(
                'header'=> $this->__('Day'),
                'index' => 'day_id',
                'renderer'  => 'Igorludgero_Advancedpermissions_Block_Adminhtml_Datetime_Renderer_Day',
            )
        );

        $this->addColumn('hour_start',
            array(
                'header'=> $this->__('Hour Start'),
                'index' => 'hour_start',
                'renderer'  => 'Igorludgero_Advancedpermissions_Block_Adminhtml_Datetime_Renderer_Time',
            )
        );

        $this->addColumn('hour_end',
            array(
                'header'=> $this->__('Hour End'),
                'index' => 'hour_end',
                'renderer'  => 'Igorludgero_Advancedpermissions_Block_Adminhtml_Datetime_Renderer_Time',
            )
        );

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/datetime/edit', array('id' => $row->getId()));
    }
}