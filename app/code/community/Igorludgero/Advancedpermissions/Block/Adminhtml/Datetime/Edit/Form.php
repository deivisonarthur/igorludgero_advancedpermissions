<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 26/08/16
 * Time: 14:00
 */

class Igorludgero_Advancedpermissions_Block_Adminhtml_Datetime_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Init class
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('igorludgero_advancedpermissions_datetime_form');
        $this->setTitle($this->__('Permissions Form'));
    }

    /**
     * Setup form fields for inserts/updates
     *
     * return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $model = Mage::registry('igorludgero_advancedpermissions');
        $adminId = $model->getAdminId();

        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method'    => 'post'
        ));

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend'    => Mage::helper('igorludgero_advancedpermissions')->__('Fill the fields to add/edit a admin permission'),
            'class'     => 'fieldset-wide',
        ));

        $fieldset->addField('admin_id', 'hidden', array(
            'name' => 'admin_id',
            'value' => $adminId,
        ));

        $fieldset->addField('day_id', 'select', array(
            'name'      => 'day_id',
            'label'     => Mage::helper('igorludgero_advancedpermissions')->__('Day'),
            'title'     => Mage::helper('igorludgero_advancedpermissions')->__('Day'),
            'values' =>
            array(
                array("value" => 1, "label" => Mage::helper('igorludgero_advancedpermissions')->__('Sunday')),
                array("value" => 2, "label" => Mage::helper('igorludgero_advancedpermissions')->__('Monday')),
                array("value" => 3, "label" => Mage::helper('igorludgero_advancedpermissions')->__('Tuesday')),
                array("value" => 4, "label" => Mage::helper('igorludgero_advancedpermissions')->__('Wednesday')),
                array("value" => 5, "label" => Mage::helper('igorludgero_advancedpermissions')->__('Thursday')),
                array("value" => 6, "label" => Mage::helper('igorludgero_advancedpermissions')->__('Friday')),
                array("value" => 7, "label" => Mage::helper('igorludgero_advancedpermissions')->__('Saturday'))
            ),
            'required'  => true,
        ));

        if ($model->getId()) {

            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
                'value' => $model->getId(),
            ));

        }

        $fieldset->addField('hour_start', 'time', array(
            'name'      => 'hour_start',
            'label'     => Mage::helper('igorludgero_advancedpermissions')->__('Hour Start'),
            'title'     => Mage::helper('igorludgero_advancedpermissions')->__('Hour Start'),
            'required'  => true,
        ));

        $fieldset->addField('hour_end', 'time', array(
            'name'      => 'hour_end',
            'label'     => Mage::helper('igorludgero_advancedpermissions')->__('Hour End'),
            'title'     => Mage::helper('igorludgero_advancedpermissions')->__('Hour End'),
            'required'  => true,
        ));

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}