<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 26/08/16
 * Time: 17:30
 */

class Igorludgero_Advancedpermissions_Block_Adminhtml_Datetime_Renderer_Time extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        $timestamp = strtotime($value);
        return date("H:i:s", $timestamp);
    }
}