<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 25/08/16
 * Time: 23:22
 */

class Igorludgero_Advancedpermissions_Model_Mysql4_Datetime_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('advancedpermissions/datetime');
    }
}