<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 25/08/16
 * Time: 23:20
 */

class Igorludgero_Advancedpermissions_Model_Datetime extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('advancedpermissions/datetime');
    }
}