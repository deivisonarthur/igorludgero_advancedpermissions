<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 26/08/16
 * Time: 14:03
 */

class Igorludgero_Advancedpermissions_Adminhtml_DatetimeController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_initAction()->renderLayout();
        //Zend_Debug::dump($this->getLayout()->getUpdate()->getHandles());
    }

    public function newAction()
    {
        // We just forward the new action to a blank edit form
        $this->_forward('edit');
    }

    public function editAction()
    {
        $this->_initAction();

        // Get id if available
        $id  = $this->getRequest()->getParam('id');
        $model = Mage::getModel('advancedpermissions/datetime');

        if ($id){
            // Load record
            $model->load($id);

            // Check if record is loaded
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This day/hour no longer exists.'));
                $this->_redirect('*/*/');

                return;
            }
        }
        else{
            $adminId = $this->getRequest()->getParam('admin_id');
            $model->setAdminId($adminId);
        }

        $this->_title($model->getId() ? $model->getName() : $this->__('New Datetime Permission'));

        $data = Mage::getSingleton('adminhtml/session')->getDatetimeData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('igorludgero_advancedpermissions', $model);

        $this->_initAction()
            ->_addBreadcrumb($id ? $this->__('Edit Datetime Permission') : $this->__('New Datetime Permission'), $id ? $this->__('Edit Datetime Permission') : $this->__('New Datetime Permission'))
            ->_addContent($this->getLayout()->createBlock('advancedpermissions/adminhtml_datetime_edit')->setData('action', $this->getUrl('*/*/save')))
            ->renderLayout();
    }

    public function saveAction()
    {

        $helper = Mage::helper("igorludgero_advancedpermissions");

        if ($postData = $this->getRequest()->getPost()) {
            $model = Mage::getModel('advancedpermissions/datetime');

            $hourStart = $postData["hour_start"][0].":".$postData["hour_start"][1].":".$postData["hour_start"][2];
            $hourEnd = $postData["hour_end"][0].":".$postData["hour_end"][1].":".$postData["hour_end"][2];

            $model->setAdminId($postData["admin_id"]);
            $model->setDayId($postData["day_id"]);
            $model->setHourStart(date('H:i:s',strtotime($hourStart)));
            $model->setHourEnd(date('H:i:s',strtotime($hourEnd)));

            if(!isset($postData["id"])) {

                if ($helper->checkCanAdd($model->getAdminId(), $model->getDayId())) {

                    if($helper->validateTimes($model->getHourStart(),$model->getHourEnd())) {

                        try {
                            $model->save();

                            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The Datetime permission has been saved.'));
                            $this->_redirect('*/permissions_user/edit/user_id/' . $model->getAdminId() . '/');

                            return;
                        } catch (Mage_Core_Exception $e) {
                            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                        } catch (Exception $e) {
                            Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while adding this new day/hour permission.'));
                        }

                    }
                    else{
                        Mage::getSingleton('adminhtml/session')->addError($this->__('Invalid Times.'));
                        $this->_redirect('*/permissions_user/edit/user_id/' . $model->getAdminId() . '/');
                    }

                    Mage::getSingleton('adminhtml/session')->setDatetimeData($postData);
                    $this->_redirectReferer();

                } else {
                    Mage::getSingleton('adminhtml/session')->addError($this->__('This admin user have a permission for this day. You can add one permission per day.'));
                    $this->_redirect('*/permissions_user/edit/user_id/' . $model->getAdminId() . '/');
                }
            }
            else{
                $model->setId($postData["id"]);
                if($helper->canChange($model->getAdminId(),$model->getId(),$model->getDayId())){

                    if($helper->validateTimes($model->getHourStart(),$model->getHourEnd())) {

                        try {
                            $model->save();

                            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The Datetime permission has been update.'));
                            $this->_redirect('*/permissions_user/edit/user_id/' . $model->getAdminId() . '/');

                            return;
                        } catch (Mage_Core_Exception $e) {
                            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                        } catch (Exception $e) {
                            Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while updating this day/hour permission.'));
                        }
                    }
                    else{
                        Mage::getSingleton('adminhtml/session')->addError($this->__('Invalid Times.'));
                        $this->_redirect('*/permissions_user/edit/user_id/' . $model->getAdminId() . '/');
                    }
                }
                else{
                    Mage::getSingleton('adminhtml/session')->addError($this->__('You cant change the day to a day that have a permission created.'));
                    $this->_redirect('*/permissions_user/edit/user_id/' . $model->getAdminId() . '/');
                }
            }
        }
    }

    public function deleteAction(){
        $id = $this->getRequest()->getParam('id');
        $permission = Mage::getModel("advancedpermissions/datetime")->load($id);
        if($permission->getAdminId()){
            $adminId = $permission->getAdminId();
            if($permission->delete()){
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Permission deleted.'));
                $this->_redirect('*/permissions_user/edit/user_id/' . $adminId . '/');
            }
        }
        else{
            Mage::getSingleton('adminhtml/session')->addError($this->__('Day/Hour permission not found.'));
            $this->_redirect('*/*/');
        }
    }

    public function messageAction()
    {
        $data = Mage::getModel('igorludgero_advancedpermissions/datetime')->load($this->getRequest()->getParam('id'));
        echo $data->getContent();
    }

    /**
     * Initialize action
     *
     * Here, we set the breadcrumbs and the active menu
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _initAction()
    {
        $this->loadLayout()->_title("DateTime Permissions");

        return $this;
    }

    /**
     * Check currently called action by permissions for current user
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        //return Mage::getSingleton('admin/session')->isAllowed('sales/foo_bar_baz');
        return true;
    }
}