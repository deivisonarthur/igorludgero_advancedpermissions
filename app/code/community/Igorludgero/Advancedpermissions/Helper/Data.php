<?php

class Igorludgero_Advancedpermissions_Helper_Data extends Mage_Core_Helper_Abstract
{

    /** Check if this admin user can add a permission for this day */
    public function checkCanAdd($admin_id,$day_id){
        $collection = Mage::getModel('advancedpermissions/datetime')->getCollection()->addFilter('admin_id',$admin_id)->addFilter("day_id",$day_id);
        if($collection->count()>0){
            return false;
        }
        return true;
    }

    /** Is this hour valid? */
    public function validateTimes($start,$end){
        if(strtotime($end)>strtotime($start)){
            return true;
        }
        return false;
    }

    /** Format Time to Array values */
    public function formaTimeToArray($time){
        return explode(":",$time);
    }

    /** Check if the admin user have day/hour permissions */
    public function checkIfHasPermissions($admin_id){
        $collection = Mage::getModel('advancedpermissions/datetime')->getCollection()->addFilter('admin_id',$admin_id);
        if($collection->count()>0){
            return true;
        }
        return false;
    }

    /** Check if can update permission with other day */
    public function canChange($admin_id,$id,$newNumberId){
        $oldPermission = Mage::getModel('advancedpermissions/datetime')->load($id);
        if($oldPermission->getDayId()==$newNumberId){
            return true;
        }
        else{
            $collection = Mage::getModel('advancedpermissions/datetime')->getCollection()->addFilter('admin_id',$admin_id)->addFilter("day_id",$newNumberId);
            if($collection->count()>0){
                return false;
            }
            return true;
        }
    }

    /** Check if admin user can log in the admin panel */
    public function canLog($admin_id){
        if($this->checkIfHasPermissions($admin_id)==false){
            return true;
        }
        else{
            $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
            $numberDay = date('w', strtotime($date))+1;
            $collection = Mage::getModel('advancedpermissions/datetime')->getCollection()->addFilter('admin_id',$admin_id)->addFilter("day_id",$numberDay);
            if($collection->count()>0){
                $item = $collection->getFirstItem();
                $start_timestamp = strtotime($item->getHourStart());
                $end_timestamp = strtotime($item->getHourEnd());
                $today_timestamp = strtotime($date);
                return (($today_timestamp >= $start_timestamp) && ($today_timestamp <= $end_timestamp));
            }
            else{
                return false;
            }
        }
    }

}